# phpspec Boilerplate 

## Prerequisites

- PHP5+
- Composer
- Nodejs + npm

## Install phpspec

Install phpspec into the project with composer: `composer install --dev`

## Install Gulp

First, make sure Gulp is installed globally:
`npm install gulp -g`

Next, install Gulp locally:
`npm install gulp gulp-phpspec gulp-notify --save-dev`

## Start a spec

Start a new project with: `vendor/phpspec/phpspec/bin/phpspec describe Acme/<project-name>`.

Run `vendor/phpspec/phpspec/bin/phpspec run` and phpspec will ask you if you want to create your new project, just press `y`. The test will be created for you in `spec/` and the class in `src/`.

## Start the test-on-save feature

Run `gulp` to start the watcher that will automatically run tests when php files have been updated.
